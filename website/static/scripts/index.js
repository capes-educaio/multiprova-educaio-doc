var docenteArea = undefined;
var discenteArea = undefined;
var gestorArea = undefined;
var adminArea = undefined;
var conteudo = undefined
var load = undefined

function init() {
  const url = new URL(window.location.href)
  const target = url.searchParams.get("target")

  load = document.getElementById("progress")
  docenteArea = document.getElementById("docente")
  discenteArea = document.getElementById("discente")
  gestorArea = document.getElementById("gestor")
  adminArea = document.getElementById("admin")
  conteudo = document.getElementById("conteudo")

  showLoad()
  mount(target)

}

function mount(target) {
  switch (target) {
    case 'docente': {
      closeLoad()
      showDocente()
      break
    }
    case 'discente': {
      closeLoad()
      showDiscente()
      break
    }
    case 'gestor': {
      closeLoad()
      showGestor()
      break
    }
    case 'admin': {
      closeLoad()
      showAdmin()
      break
    }
    default: {
      closeLoad()
      showDocente()
    }
  }
}

function showDocente() {
  conteudo.innerHTML = docenteArea.innerHTML
}

function showDiscente() {
  conteudo.innerHTML = discenteArea.innerHTML
}

function showGestor() {
  conteudo.innerHTML = gestorArea.innerHTML
}

function showAdmin() {
  conteudo.innerHTML = adminArea.innerHTML
}

function showLoad() {
  load.style.display = 'active'
}

function closeLoad() {
  load.style.display = 'none'
}

window.addEventListener("DOMContentLoaded", () => {
  init()
})